from multiprocessing.dummy import Pool as ThreadPool
import multiprocessing, time


def mainfunc(num):
    starttime = time.time()
    s = 1
    for i in range(1, num):
        s *= i

    endtime = time.time()
    return "耗时：{0}".format(endtime-starttime)


if __name__ == '__main__':
    pool = ThreadPool(multiprocessing.cpu_count())
    listdata = [200000, 200000, 200000, 200000, 200000, 200000, 200000, 200000]
    result = pool.map(mainfunc, listdata)
    pool.close()
    pool.join()
    print(result)